Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("----index on load-------");
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    console.log("----index on ready-------");
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log("----index on show-------");
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    console.log("----index on Hide-------");
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    console.log("----index on Unload-------");
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log("----index on PullDownFresh-------");
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

    console.log("----index on ReachBottom-------");
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    console.log("----index on Share-------");
    
  },
  itemClick:function(){
   
    wx.navigateTo({
      url: '../detail/detail?id=100'
    });
    // console.log("itemclick");
  }
})